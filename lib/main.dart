import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import 'package:pokedex/app/stores/pokeapi.dart';
import 'package:pokedex/app/views/home_page.dart';

void main() {
  // Creates an GetIt instance and registers a PokeApiStore to it
  GetIt getIt = GetIt.instance;
  getIt.registerSingleton<PokeApiStore>(PokeApiStore());

  // Initializes the main app
  return runApp(Pokedex());
}
