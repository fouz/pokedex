import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';

import 'package:pokedex/app/models/pokemon_model.dart';
import 'package:pokedex/app/stores/pokeapi.dart';
import 'package:pokedex/app/views/pokemon_page.dart';

class Pokedex extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pokédex',
      home: PokemonList(),
    );
  }
}

class PokemonList extends StatefulWidget {
  @override
  _PokemonListState createState() => _PokemonListState();
}

class _PokemonListState extends State<PokemonList> {
  // Base URL of the Pokémon sprites
  static const String baseImageUrl =
      "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/";

  PokeApiStore _pokeApiStore;

  @override
  void initState() {
    super.initState();
    _pokeApiStore = GetIt.instance<PokeApiStore>();
    _pokeApiStore.fetchPokemonList(0);
  }

  Widget _buildPokemonList() {
    return Observer(
      builder: (BuildContext context) {
        List<Pokemon> _pokemonList = _pokeApiStore.pokemonList;
        return (_pokemonList != null)
            ? ListView.builder(
                itemCount: _pokemonList.length,
                itemBuilder: (context, index) {
                  if (index >= _pokemonList.length) {
                    _pokemonList.addAll(_pokeApiStore.fetchPokemonList(0));
                  }
                  return Card(
                    child: ListTile(
                      leading: CachedNetworkImage(
                          imageUrl:
                              baseImageUrl + (index + 1).toString() + ".png"),
                      title: // First letter of the Pokémon name uppercase
                          Text(_pokemonList[index]
                                  .name
                                  .substring(0, 1)
                                  .toUpperCase() +
                              _pokemonList[index]
                                  .name
                                  .substring(1)
                                  .toLowerCase()),
                      subtitle:
                          // Formats the string into "#nnn"
                          Text("#" + (index + 1).toString().padLeft(3, "0")),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) =>
                                PokemonDetailsPage(
                              name: _pokemonList[index].name,
                            ),
                            fullscreenDialog: true,
                            maintainState: false,
                          ),
                        );
                      },
                    ),
                  );
                },
              )
            : Center(
                child: CircularProgressIndicator(),
              );
      },
    );
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pokédex'),
      ),
      body: _buildPokemonList(),
    );
  }
}
