import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';

import 'package:pokedex/app/models/pokemon_details_model.dart';
import 'package:pokedex/app/models/pokemon_species_model.dart';
import 'package:pokedex/app/stores/pokeapi.dart';

class PokemonDetailsPage extends StatefulWidget {
  final String name;

  PokemonDetailsPage({Key key, this.name}) : super(key: key);

  @override
  _PokemonDetailsPageState createState() => _PokemonDetailsPageState();
}

class _PokemonDetailsPageState extends State<PokemonDetailsPage> {
  PokeApiStore _pokeApiStore;

  // Gather the Pokémon base stats and returns a vector representing them
  List<int> getPokemonStats(PokemonDetails pokemonStats) {
    // list[0] = Speed
    // list[1] = Special defense
    // list[2] = Special attack
    // list[3] = Defense
    // list[4] = Attack
    // list[5] = HP
    List<int> list = [0, 0, 0, 0, 0, 0];

    pokemonStats.stats.forEach((item) {
      switch (item.stat.name) {
        case 'speed':
          list[0] = item.baseStat;
          break;
        case 'special-defense':
          list[1] = item.baseStat;
          break;
        case 'special-attack':
          list[2] = item.baseStat;
          break;
        case 'defense':
          list[3] = item.baseStat;
          break;
        case 'attack':
          list[4] = item.baseStat;
          break;
        case 'hp':
          list[5] = item.baseStat;
          break;
      }
    });

    return list;
  }

  // Gets Pokémon types from a PokemonDetails object and returns a String
  // representing them
  String getPokemonTypes(PokemonDetails pokemonDetails) {
    String data = "";

    pokemonDetails.types.forEach((item) {
      data = data +
          item.type.name.substring(0, 1).toUpperCase() +
          item.type.name.substring(1).toLowerCase() +
          ", ";
    });

    return data.substring(0, (data.length - 2));
  }

  // Gets Pokémon abilities from a PokemonDetails object and returns a String
  // representing them
  String getPokemonAbilities(PokemonDetails pokemonDetails) {
    String data = "";

    pokemonDetails.abilities.forEach((item) {
      data = data +
          item.ability.name.substring(0, 1).toUpperCase() +
          item.ability.name.substring(1).toLowerCase() +
          ", ";
    });

    return data.substring(0, (data.length - 2));
  }

  @override
  void initState() {
    _pokeApiStore = GetIt.instance<PokeApiStore>();
    _pokeApiStore.getPokemonDetails(widget.name);
    _pokeApiStore.getPokemonSpecies(widget.name);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(""),
        ),
        body: Observer(builder: (context) {
          PokemonDetails _pokemonDetails = _pokeApiStore.pokemonDetails;
          PokemonSpecies _pokemonSpecies = _pokeApiStore.pokemonSpecies;
          List<int> _statsList = getPokemonStats(_pokeApiStore.pokemonDetails);
          return ListView(
            children: <Widget>[
              Card(
                child: CachedNetworkImage(
                  imageUrl: _pokemonDetails.sprites.frontDefault,
                ),
              ),
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      // First letter of the Pokémon name uppercase
                      Text(
                        _pokemonDetails.name.substring(0, 1).toUpperCase() +
                            _pokemonDetails.name.substring(1).toLowerCase(),
                        style: TextStyle(fontSize: 24),
                      ),
                      // Formats the string into "#nnn"
                      Text(
                        "#" + _pokemonDetails.id.toString().padLeft(3, "0"),
                        style: TextStyle(fontSize: 16, color: Colors.grey),
                      ),
                    ],
                  ),
                ),
              ),
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        getPokemonTypes(_pokemonDetails),
                        style: TextStyle(fontSize: 18),
                      ),
                      Text(
                        "Type",
                        style: TextStyle(color: Colors.grey),
                      ),
                    ],
                  ),
                ),
              ),
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  _pokemonDetails.baseExperience.toString(),
                                  style: TextStyle(fontSize: 18),
                                ),
                                Text(
                                  "Base EXP",
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  // Convert the Pokémon weight in hectograms to kilograms
                                  // kilogram = hectogram / 10
                                  (_pokemonDetails.weight / 10).toString() +
                                      " kg",
                                  style: TextStyle(fontSize: 18),
                                ),
                                Text(
                                  "Weight",
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  // Convert the Pokémon height in decimetres to metres
                                  // meter = decimeter / 10
                                  (_pokemonDetails.height / 10).toString() +
                                      " m",
                                  style: TextStyle(fontSize: 18),
                                ),
                                Text(
                                  "Height",
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Padding(padding: const EdgeInsets.all(12.0)),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  _statsList[0].toString(),
                                  style: TextStyle(fontSize: 18),
                                ),
                                Text(
                                  "Speed",
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  _statsList[1].toString(),
                                  style: TextStyle(fontSize: 18),
                                ),
                                Text(
                                  "Special defense",
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  _statsList[2].toString(),
                                  style: TextStyle(fontSize: 18),
                                ),
                                Text(
                                  "Special attack",
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      Padding(padding: const EdgeInsets.all(12.0)),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  _statsList[3].toString(),
                                  style: TextStyle(fontSize: 18),
                                ),
                                Text(
                                  "Defense",
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  _statsList[4].toString(),
                                  style: TextStyle(fontSize: 18),
                                ),
                                Text(
                                  "Attack",
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  _statsList[5].toString(),
                                  style: TextStyle(fontSize: 18),
                                ),
                                Text(
                                  "HP",
                                  style: TextStyle(color: Colors.grey),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        getPokemonAbilities(_pokemonDetails),
                        style: TextStyle(fontSize: 18),
                      ),
                      Text(
                        "Abilities",
                        style: TextStyle(color: Colors.grey),
                      ),
                    ],
                  ),
                ),
              ),
              (_pokemonSpecies != null)
                  ? Card(
                      child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Text(_pokemonSpecies.flavorTextEntries
                          .where((item) => item.language.name == 'en')
                          .first
                          .flavorText
                          .replaceAll("\n", " ")),
                    ))
                  : Center(
                      child: Padding(
                        padding: const EdgeInsets.all(24),
                        child: CircularProgressIndicator(),
                      ),
                    )
            ],
          );
        }));
  }
}
