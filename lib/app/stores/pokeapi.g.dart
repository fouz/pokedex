// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pokeapi.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PokeApiStore on _PokeApiStoreBase, Store {
  final _$pokemonListAtom = Atom(name: '_PokeApiStoreBase.pokemonList');

  @override
  List<Pokemon> get pokemonList {
    _$pokemonListAtom.reportRead();
    return super.pokemonList;
  }

  @override
  set pokemonList(List<Pokemon> value) {
    _$pokemonListAtom.reportWrite(value, super.pokemonList, () {
      super.pokemonList = value;
    });
  }

  final _$pokemonDetailsAtom = Atom(name: '_PokeApiStoreBase.pokemonDetails');

  @override
  PokemonDetails get pokemonDetails {
    _$pokemonDetailsAtom.reportRead();
    return super.pokemonDetails;
  }

  @override
  set pokemonDetails(PokemonDetails value) {
    _$pokemonDetailsAtom.reportWrite(value, super.pokemonDetails, () {
      super.pokemonDetails = value;
    });
  }

  final _$pokemonSpeciesAtom = Atom(name: '_PokeApiStoreBase.pokemonSpecies');

  @override
  PokemonSpecies get pokemonSpecies {
    _$pokemonSpeciesAtom.reportRead();
    return super.pokemonSpecies;
  }

  @override
  set pokemonSpecies(PokemonSpecies value) {
    _$pokemonSpeciesAtom.reportWrite(value, super.pokemonSpecies, () {
      super.pokemonSpecies = value;
    });
  }

  final _$getPokemonDetailsAsyncAction =
      AsyncAction('_PokeApiStoreBase.getPokemonDetails');

  @override
  Future<void> getPokemonDetails(String name) {
    return _$getPokemonDetailsAsyncAction
        .run(() => super.getPokemonDetails(name));
  }

  final _$getPokemonSpeciesAsyncAction =
      AsyncAction('_PokeApiStoreBase.getPokemonSpecies');

  @override
  Future<void> getPokemonSpecies(String name) {
    return _$getPokemonSpeciesAsyncAction
        .run(() => super.getPokemonSpecies(name));
  }

  final _$_PokeApiStoreBaseActionController =
      ActionController(name: '_PokeApiStoreBase');

  @override
  dynamic fetchPokemonList(dynamic offset) {
    final _$actionInfo = _$_PokeApiStoreBaseActionController.startAction(
        name: '_PokeApiStoreBase.fetchPokemonList');
    try {
      return super.fetchPokemonList(offset);
    } finally {
      _$_PokeApiStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
pokemonList: ${pokemonList},
pokemonDetails: ${pokemonDetails},
pokemonSpecies: ${pokemonSpecies}
    ''';
  }
}
