import 'dart:convert';

import 'package:mobx/mobx.dart';
import 'package:http/http.dart' as http;

import 'package:pokedex/app/models/pokemon_model.dart';
import 'package:pokedex/app/models/pokemon_details_model.dart';
import 'package:pokedex/app/models/pokemon_species_model.dart';

part 'pokeapi.g.dart';

class PokeApiStore = _PokeApiStoreBase with _$PokeApiStore;

abstract class _PokeApiStoreBase with Store {
  // The base PokéAPI v2 URL
  static const String baseUrl = "https://pokeapi.co/api/v2";

  // We're only getting the first generation Pokémon
  static const int limit = 151;

  @observable
  List<Pokemon> pokemonList;

  @observable
  PokemonDetails pokemonDetails;

  @observable
  PokemonSpecies pokemonSpecies;

  @action
  fetchPokemonList(offset) {
    getPokemonList(offset).then((list) {
      pokemonList = list;
    });
  }

  Future<List<Pokemon>> getPokemonList(int offset) async {
    try {
      final response = await http.get(baseUrl +
          "/pokemon?limit=" +
          limit.toString() +
          "&offset=" +
          offset.toString());
      List<Pokemon> list = new List<Pokemon>();
      var decodeJson = jsonDecode(response.body);
      for (var item in (decodeJson['results'] as List)) {
        list.add(Pokemon.fromJson(item));
      }
      return list;
    } catch (error) {
      print("Error fetching Pokémon list");
      return null;
    }
  }

  @action
  Future<void> getPokemonDetails(String name) async {
    try {
      final response = await http.get(baseUrl + "/pokemon/" + name);
      var decodeJson = jsonDecode(response.body);
      pokemonDetails = PokemonDetails.fromJson(decodeJson);
    } catch (error, stacktrace) {
      print("Error fetching Pokémon details" + stacktrace.toString());
    }
  }

  @action
  Future<void> getPokemonSpecies(String name) async {
    try {
      pokemonSpecies = null;
      final response = await http.get(baseUrl + "/pokemon-species/" + name);
      var decodeJson = jsonDecode(response.body);
      pokemonSpecies = PokemonSpecies.fromJson(decodeJson);
    } catch (error, stacktrace) {
      print("Error fetching Pokémon species" + stacktrace.toString());
    }
  }
}
