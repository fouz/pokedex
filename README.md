# Pokédex

Pokédex is an application for Pokémon consultation, developed using the Flutter framework and the PokéAPI v2 RESTful API.

## Features

- List all the first generation Pokémon
- Show the selected Pokémon details (types, base stats, weight, height, abilities and description)

## Screenshots

![Home](doc/screenshots/home.png "Home")
![Details](doc/screenshots/details.png "Details")

## Dependencies

The following dependencies are used in this project: 

- `mobx` and `flutter_mobx` for state management
- `http` for http requests
- `get_it` for instancing
- `cached_network_image` to download and cache Pokémon images

Additionally, the following dependencies are used in the development environment: 

- `build_runner`
- `mobx_codegen`

All the necessary depencencies can be downloaded using the command `flutter pub get` as described below.

## Getting Started

- Install [Flutter](https://flutter.dev/docs/get-started/install) in your machine

- Open this project folder in your Terminal application and run `flutter pub get` to download the necessary dependencies

- Run `flutter packages pub run build_runner build` to build the necessary MobX modules

- Run `flutter run` to run the application on your emulator/phone of choice

- While developing, it's recommended to run the command `flutter packages pub run build_runner watch` to build the MobX modules as you edit the code.